newobjs = instrfind;
if newobjs ~= []
    fclose(newobjs);
end

serialPort = 'COM10';           % define COM port #
baudeRate = 9600;
commandsKeys = {'RESET', 'START_SENDING_DATA', 'STOP_EXPERIMENT', 'START_EXPERIMENT', 'CHANGE_DIR', 'UPDATE_PERIOD'};
commandsValues = [45, 50, 55, 60, 65, 70];
commands = containers.Map(commandsKeys, commandsValues);
%after sending any commands there must be a small pause, like pause(0.02)

logFileName='pendulum.log';
logfid = fopen(logFileName, 'w+');

pause(2);
s = serial(serialPort, 'BaudRate',baudeRate);
fopen(s);

pause(1.5);
fwrite(s, [commands('START_SENDING_DATA'), 0]);
experimentIsStarted = false;

while(1)
    data = fread(s, 24, "uint8");
    convertedData = typecast( uint8(data) , 'single');
    if(~isempty(convertedData) && length(convertedData)== 6)
        time = convertedData(1);
        pos = convertedData(2); 
        spd = convertedData(3);
        angl  = convertedData(4) * 57.32;
        anglspd  = convertedData(5) * 57.32;
        control = convertedData(6);
        
        %insert any logic to obtain control for sending

        logstr = [num2str(time) ', ' num2str(pos) ', ' num2str(spd) ', ' num2str(angl) ', ' num2str(anglspd) ', ' num2str(control) newline];
        fwrite(logfid, logstr);
        disp(sprintf('Time: %f\tPos: %f\tSpd: %f\tAngle: %f\tAngle Speed: %f\tCtrl: %f\t\n', time, pos, spd, angl, anglspd, control));
    end
end